# Getting Started

This Spring Boot project exposes multiple profiles that can be used to test out different connectivity patterns with the Jedis and Lettuce Redis clients.  It provides a GET endpoint at http://{IP}:8081/examples/key/{key}

## Patterns

All profiles can be set by exporting the profile into the environment variable spring_profiles_active.  For example: `export spring_profiles_active=jedis` to run the jedis profile

### jedis
This profile uses a RedisTemplate with a JedisConnectionFactory

### jedis-cf
This profile uses a JedisConnectionFactory

### jedis-rf
This profile uses a RetryableJedisConnectionFactory (for the instances when a connection goes stale).  The retry logic was borrowed from https://gist.github.com/derylspielman/7cee44e216475d498ba3819702629a17

### lettuce
This profile uses a RedisTemplate with a LettuceConnectionFactory

### lettuce-cf
This profile uses a LettuceConnectionFactory

## Installing

### Maven
You can import the project into your favourite ide

### Local docker environment
You can also run the provided docker environment.  It comes with an IDE (VSCode) and a local redis instance.

To bring up:

- `docker-compose up -d --build`


You can now browse to port 80 on your browser (e.g. http://localhost/)  
In the resulting VS Code UI, enter "clients" as password  
Click "Open Folder", select "project", and click OK

To open a terminal, click the three bars on the top left, select Terminal, then New Terminal.  You can reach the local redis instance by typing redis-cli in the terminal window

You can access the Locust endpoint at http://{IP}/locust.  This endpoint will simulate usage.  A basic script is provided in scripts/locust.py.  You can modify to suit your needs.  Use a suitable number of threads so as to not overwhelm your machine.

### Run Spring Boot App

In your terminal window

- `export spring_profiles_active=jedis` (default is jedis if this environment variable is not supplied)
- `mvn spring-boot:run`

Port 8081 should now be active
