package com.redislabs.examples.redis.service;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisClientsService {
	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	@Autowired
	private RedisConnectionFactory connectionFactory;

	@Value("${app.redis.cache-expiration-minutes}")
	public int redisCacheExpirationInMinutes;

	@Value("${app.use-cf}")
	private boolean useConnectionFactory;

	public String add(String key, String value) {
		this.redisTemplate.opsForValue().set(key, value, Duration.ofMinutes(redisCacheExpirationInMinutes));
		return key;
	}

	public String get(String key) {
		String value = "";
		value = useConnectionFactory ? getFromConnectionFactory(key) : getFromRedisTemplate(key);
		return value;
	}

	private String getFromConnectionFactory(String key) {
		RedisConnection connection = this.connectionFactory.getConnection();
		System.out.println("Retrieving using connection from " + connectionFactory);
		byte[] response = connection.get(key.getBytes());
		connection.close();
		return response == null ? null : new String(response);
	}

	public String getFromRedisTemplate(String key) {
		System.out.println("Retrieving " + key + " using template from " + redisTemplate.getConnectionFactory());
		return this.redisTemplate.opsForValue().get(key);
	}
}